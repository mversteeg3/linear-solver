import main.Scheduling_Model.ScheduleFactory;
import main.Scheduling_Model.Schedule;

public class Program {

	public static void main(String[] arg) 
	{
		try
		{
			Schedule problem = ScheduleFactory.createSchedule();
			GurobiSolver solver = new GurobiSolver();
			solver.Solve(problem);
		}
		catch(Exception e)
		{
			System.out.println("Error occurred: " + e.getMessage());
		}
	}
}
