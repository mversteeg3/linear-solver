import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;
import gurobi.GRBVar;
import main.MIP_Model.Common.EnVariableType;
import main.MIP_Model.Constraint;
import main.MIP_Model.ISolver;
import main.MIP_Model.Problem;
import main.MIP_Model.Term;
import main.MIP_Model.Variable;


public class GurobiSolver implements ISolver
{	
	private GRBEnv _env;
	private GRBModel _model;
	private Problem _prob;

	public void Solve(Problem p)
	{
		try
		{
			_prob = p;
			_env = new GRBEnv("scheduleProblem.log");
			_model = new GRBModel(_env);
			
			addVariables();
			addConstraints();
			addObjective();
			
			_model.update();
			_model.optimize();
			_model.update();
			_model.write("/home/ubuntu/Desktop/output.lp");
			for(Term t : _prob.objective.getTerms())
			{
				System.out.println("Value of " + t.getVariableName()+": "+ _model.getVarByName(t.getVariableName()).get(GRB.DoubleAttr.X));
			}
		}
		catch(Exception e)
		{
			System.out.println("FAILED: " + e.getMessage());
		}
	}

	private Boolean addVariables() throws GRBException {
		// Add variables
		for (Variable var : _prob.variables) {
			if (var.type == EnVariableType.Boolean)
				_model.addVar(var.lowerBound, var.upperBound, var.objective,
						GRB.BINARY, var.name);
			else if (var.type == EnVariableType.Integer)
				_model.addVar(var.lowerBound, var.upperBound, var.objective,
						GRB.INTEGER, var.name);
			else
				_model.addVar(var.lowerBound, var.upperBound, var.objective,
						GRB.CONTINUOUS, var.name);
		}

		_model.update();
		return true;
	}
	
	private Boolean addConstraints() throws GRBException
	{
		// Add constraints
		for(Constraint constraint : _prob.constraints)
		{
			//System.out.println("Constraint: " + constraint.getName());
			GRBLinExpr expr = new GRBLinExpr();
			for(Term t : constraint.getTerms())
			{
				//System.out.println(t.getVariableName());
				GRBVar var = _model.getVarByName(t.getVariableName());
				if(var == null)
					return false;
				expr.addTerm(t.getCoefficient(), var);
			}
			switch(constraint.getComparator()) 
			{
				case EqualTo:
					_model.addConstr(expr, GRB.EQUAL, constraint.getRHS(), constraint.getName());
					break;
				case GreaterThanOrEqualTo:
					_model.addConstr(expr, GRB.GREATER_EQUAL, constraint.getRHS(), constraint.getName());
					break;
				case LessThanOrEqualTo:
					_model.addConstr(expr, GRB.LESS_EQUAL, constraint.getRHS(), constraint.getName());
					break;
				default:
					return false;
			}
		}
		return true;
	}
	
	private Boolean addObjective() throws GRBException
	{
		GRBLinExpr expr = new GRBLinExpr();
		for(Term t : _prob.objective.getTerms())
		{
			expr.addTerm(t.getCoefficient(), _model.getVarByName(t.getVariableName()));
		}
		switch(_prob.objective.getOptimizationGoal())
		{
			case Maximize:
				_model.setObjective(expr, GRB.MAXIMIZE);
				break;
			case Minimize:
				_model.setObjective(expr, GRB.MINIMIZE);
				break;
		}
		return true;
	}

}
