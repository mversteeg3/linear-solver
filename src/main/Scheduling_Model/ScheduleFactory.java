package main.Scheduling_Model;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ScheduleFactory {

	private static int _numCourses;
	private static int _numStudents;
	private static int _numSemesters;
	private static BufferedReader _reader;
	
	public static Schedule createSchedule()
	{
		JFileChooser chooser = new JFileChooser();
	    FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt");
	    chooser.setFileFilter(filter);
	    int returnVal = chooser.showOpenDialog(null);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	    	return createSchedule(chooser.getSelectedFile().getAbsolutePath());
	    }
	    System.out.println("No file selected.");
	    return null;
	}
	
	public static Schedule createSchedule(String fileName)
	{
		_numStudents = 0;
		_numCourses = 0;
		_numSemesters = 0;
		
		Schedule schedule = new Schedule();
		ArrayList<Student> allStudents = new ArrayList<Student>();
		
		try
		{
			_reader = new BufferedReader( new FileReader(fileName) );		
			int[] studentDesiredCourses;
			while((studentDesiredCourses = ReadSingleLine()) != null)
			{
				ArrayList<Course> courses = new ArrayList<Course>();
				for(int courseNum : studentDesiredCourses)
				{
					if(!schedule.courseList.containsKey(courseNum))
					{
						System.out.println("Course number " + courseNum + " does not exist.");
						return null;
					}
					courses.add(schedule.courseList.get(courseNum));
				}
				Student newStudent = new Student(_numStudents);
				
				newStudent.desiredCourses = (Course[]) courses.toArray(new Course[courses.size()]);
				allStudents.add(newStudent);
				_numStudents++;
			}
			_reader.close();
			schedule.studentList = allStudents.toArray(new Student[allStudents.size()]);
			
			schedule.numCourses = _numCourses;
			schedule.numSemesters = _numSemesters;
			schedule.numStudents = _numStudents;
			schedule.buildProblem();
			return schedule;
		}
		catch(Exception e)
		{
			System.out.println("Error occurred while reading input file: " + e.getMessage());
			return null;
		}
	}

	
	private static int[] ReadSingleLine()
	{
		try
		{
			Boolean validLine = false;
			String line;
			do
			{
				line = _reader.readLine();
				if(line == null)
					return null;
				
				// Remove whitespace
				line = line.replaceAll(" ", "");
				line = line.replaceAll("\t", "");
				
				// Skip lines that begin with %
				if(!line.startsWith("%") && !line.equals(""))
					validLine = true;
				
			}while (!validLine);	// Loop until a valid line is found (Not a comment or blank line).
			
			String[] courseNumberStrings = line.split("\\.");
			int numDesiredCourses = courseNumberStrings.length;
			if(_numSemesters == 0)
			{
				_numSemesters = numDesiredCourses;
				if(_numSemesters != Constants.NUM_SEMESTERS)
				{
					System.out.println("Error: the number of semesters do not match expected value of " + Constants.NUM_SEMESTERS);
					return null;
				}
			}
			if(numDesiredCourses != _numSemesters)
			{
				System.out.println("Error: The number of desired courses for student " + _numStudents + " does not match the number of semesters.  Row will be excluded");
				return null;
			}
			
			int[] courseNumbers = new int[numDesiredCourses];
			for(int i = 0; i < numDesiredCourses; i++)
			{
				courseNumbers[i] = Integer.parseInt(courseNumberStrings[i]);
				
				// The number of courses should equal the highest course number in the input file.  
				// That is to say, if there exists a course number 18, there must be 18 courses total.
				if(courseNumbers[i] > _numCourses)
				{
					_numCourses = courseNumbers[i];
				}
			}
			return courseNumbers;
		}
		catch(Exception e)
		{
			System.out.println("Exception occurred while reading input file for student number " +_numStudents + ":  " + e.getMessage());
			return null;
		}
	}

	
	
//	public static Semester[] getSemesterList(EnSemesterType type)
//	{
//		return _semesterLists.get(type);
//	}
	
}
