package main.Scheduling_Model;

import main.Scheduling_Model.Constants.EnSemesterType;

public class Semester {

	int number;
	private EnSemesterType type;
	
	public Semester(int num)
	{
		number = num;
		
		// Determine which term this is
		switch((number + Constants.FIRST_SEMESTER) % Constants.NUM_SEMESTER_TYPES)
		{
			case Constants.FALL_SEMESTER:
				type = EnSemesterType.Fall;
				break;
			case Constants.SPRING_SEMESTER:
				type = EnSemesterType.Spring;
				break;
			case Constants.SUMMER_SEMESTER:
				type = EnSemesterType.Summer;
				break;
			default:
				System.out.println("Invalid Semester Type");
				break;
		}
	}
	
	public EnSemesterType getSemesterType()
	{
		return type;
	}
	
}
