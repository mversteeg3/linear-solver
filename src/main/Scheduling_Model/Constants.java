package main.Scheduling_Model;

public class Constants 
{
	
	public enum EnSemesterType
	{
		Fall,
		Spring,
		Summer,
		All
	};
	
	public static final int NUM_COURSES = 18;
	public static final int NUM_SEMESTERS = 12;
	
	public static final int NUM_SEMESTER_TYPES = 3;
	public static final int FALL_SEMESTER   = 0;
	public static final int SPRING_SEMESTER = 1;
	public static final int SUMMER_SEMESTER = 2;
	public static final int FIRST_SEMESTER = FALL_SEMESTER;
	
	public static final int MAX_COURSE_PER_SEMESTER = 2;

}
