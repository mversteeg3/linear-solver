package main.Scheduling_Model;

import java.util.Hashtable;

import main.MIP_Model.BooleanVariable;
import main.MIP_Model.Common.EnComparator;
import main.MIP_Model.Common.EnOptimization;
import main.MIP_Model.Constraint;
import main.MIP_Model.IntegerVariable;
import main.MIP_Model.Objective;
import main.MIP_Model.Problem;
import main.MIP_Model.Variable;
import main.Scheduling_Model.Constants.EnSemesterType;

public class Schedule extends Problem
{	
	private BooleanVariable[][][] _variables;
	private IntegerVariable _maxSizeVariable;
	
	public int numSemesters;
	public int numStudents;
	public int numCourses;
	
	public Student[] studentList;
	public Hashtable<Integer, Course> courseList = new Hashtable<Integer, Course>();
	public Semester[] semesterList = new Semester[Constants.NUM_SEMESTERS];

	Schedule()
	{
		super("ScheduleProblem");
		initializeSemesterList();
		initializeCourseListing();
	}
	
	private void initializeCourseListing()
	{
		for(int i = 1; i <= Constants.NUM_COURSES; i++)
		{
			Course c = new Course(i, ConstraintDefinitions.SemestersOffered.get(i));
			c.PopulateSemestersOffered(semesterList);
			courseList.put(i, c);
		}
		for(int i = 1; i <= Constants.NUM_COURSES; i++)
		{
			courseList.get(i).populatePrerequisiteList(courseList);
		}
	}	
	
	private void initializeSemesterList()
	{
		for(int i = 0; i < Constants.NUM_SEMESTERS; i++)
		{
			semesterList[i]= new Semester(i);
		}
	}	
	
	public void buildProblem() 
	{	
		if(studentList == null)
			return;
		if(!initializeVariables())
			return;
		if(!createMaxCoursePerSemesterConstraints())
			return;
		if(!createNoDuplicateClassesConstraints())
			return;
		if(!createCourseSizeConstraints())
			return;
		if(!createPrerequisitesConstraints())
			return;
	}

	private Boolean initializeVariables()
	{
		// Construct all variables
		_variables = new BooleanVariable[numStudents][numCourses][numSemesters];
		_maxSizeVariable = new IntegerVariable("MaxCourseSize");
		_maxSizeVariable.upperBound = numStudents;
		variables.add(_maxSizeVariable);
		
		for(Student student : studentList)
		{
			for (Course c: student.desiredCourses)
			{
				for(Semester semester : c.semestersOffered)
				{
					BooleanVariable newVar = new BooleanVariable("S" + student.number +"_C" + c.number + "_T" + semester.number);
					variables.add(newVar);
					_variables[student.number][c.number-1][semester.number] = newVar;
					
				}
			}
		}
		
		// Construct objective
		Objective classSizeObjective = new Objective(EnOptimization.Minimize);
		classSizeObjective.AddTerm(1, _maxSizeVariable);
		objective = classSizeObjective;
		
		return true;
	}
	
	
	// Construct max courses per student per semester constraint and adds to the
	// problem definition
	private Boolean createMaxCoursePerSemesterConstraints() 
	{
		for (Student student : studentList) {
			for (Semester semester : semesterList) {
				
				Constraint newConstraint = new Constraint("MaxCoursePerSemester_S" + student.number + "_C"+ "X" + "_T" + semester,
															Constants.MAX_COURSE_PER_SEMESTER,
															EnComparator.LessThanOrEqualTo);

				// Loop through all of the courses this student wants to take
				for (Course c : student.desiredCourses) 
				{

					// Only add the variable to the constraint if the course is
					// offered for the current semester.
					if (IsCourseOfferedForSemester(c, semester.number)) 
					{

						// Get the variable representing whether the student is
						// taking course c during the semester.
						Variable var = getVariable(student, c, semester);
						if (var == null) 
						{
							System.out.println("Max Courses Error: No variable is defined at (" + student.number+ "," + (c.number - 1) + "," + semester + ")");
							return false;
						}
						// Add the term to the constraint
						newConstraint.AddTerm(1, var);
					}
				}
				// Add the constraint to the problem.
				constraints.add(newConstraint);
			}
		}
		return true;
	}
	
	// Construct no duplicate courses per student constraint
	private Boolean createNoDuplicateClassesConstraints() 
	{
		
		//  Loop through each student
		for (Student student : studentList) {
			
			//  Loop through the student's desired courses
			for (Course c : student.desiredCourses) {
				
				Constraint newConstraint = new Constraint("DuplicateCourse_"
						+ student.number + "_" + c.number + "_X", 1,
						EnComparator.EqualTo);
				
				// Loop through all semesters
				for (Semester semester : c.semestersOffered) {
					
						Variable var = getVariable(student, c, semester);
						if (var == null) {
							System.out
									.println("Duplicate Courses Error: No variable is defined at ("
											+ student.number
											+ ","
											+ (c.number - 1)
											+ ","
											+ semester + ")");
							return false;
						}
						newConstraint.AddTerm(1, var);
					}
				
				constraints.add(newConstraint);
			}
		}
		return true;
	}

	private Boolean createPrerequisitesConstraints() 
	{
		for (Student student : studentList) {
			for (Course c : student.desiredCourses) {
				for (Course prerequisite : c.prerequisites) {
					for (Semester semester : c.semestersOffered)
					{
						Constraint newConstraint = new Constraint("Prerequisites_S" + student.number + "_C"
										+ c.number + "(P"
										+ prerequisite + ")_T" + semester,
								0, EnComparator.GreaterThanOrEqualTo);
						Variable var = getVariable(student, c, semester);
						if (var == null) {
							System.out
									.println("Prerequisites Error: No variable is defined at ("
											+ student.number
											+ ","
											+ (c.number - 1)
											+ ","
											+ semester + ")");
							return false;
						}
						newConstraint.AddTerm(-1, var);
						for (int prereqSemesterNumber = 0; prereqSemesterNumber < semester.number; prereqSemesterNumber++) 
						{
							if (IsCourseOfferedForSemester(prerequisite, prereqSemesterNumber)) 
							{
								var = getVariable(student, prerequisite, semesterList[prereqSemesterNumber]);
								if (var == null) 
								{
									System.out.println("Prerequisite Error: No variable is defined at ("
													+ student.number
													+ ","
													+ (prerequisite.number - 1)
													+ ","
													+ prereqSemesterNumber
													+ ")");
									return false;
								}
								newConstraint.AddTerm(1, var);
							}
						}
						constraints.add(newConstraint);
					}
					

				}
			}
		}
		return true;
	}
	
	private Boolean createCourseSizeConstraints()
	{
		// Construct class size constraint
		for(int semester = 0; semester < numSemesters; semester++) 
		{
			for (int course = 1; course <= numCourses; course++)
			{
				if(IsCourseOfferedForSemester(courseList.get(course), semester))
				{
					Constraint newConstraint = new Constraint("CourseSize_X_"
							+ course + "_" + semester, 0,
							EnComparator.LessThanOrEqualTo);
					newConstraint.AddTerm(-1, _maxSizeVariable);
					for (Student student : studentList) 
					{
						if (!student.RequiresCourse(course))
							continue;
						Variable var = _variables[student.number][course - 1][semester];
						if (var == null) 
						{
							System.out
									.println("Class size Error: No variable is defined at ("
											+ student.number
											+ ","
											+ (course - 1)
											+ ","
											+ semester
											+ ")");
							return false;
						}
						newConstraint.AddTerm(1, var);
					}
					constraints.add(newConstraint);
				}
			}
		}
		return true;
	}

	private Boolean IsCourseOfferedForSemester(Course course, int semester)
	{
		return course.whenOffered == EnSemesterType.All || course.whenOffered == semesterList[semester].getSemesterType();
	}
		
	public Variable getVariable(int studentNum, int courseNum, int semesterNum)
	{
		// TODO: Course number logic should be generalized 
		return _variables[studentNum][courseNum - 1][semesterNum];
	}
	
	public Variable getVariable(Student student, Course course, Semester semester)
	{
		// TODO: Course number logic should be generalized 
		return getVariable(student.number, course.number, semester.number);
	}

}
