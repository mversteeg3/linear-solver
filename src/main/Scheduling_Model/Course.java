package main.Scheduling_Model;

import java.util.ArrayList;
import java.util.Hashtable;

import main.Scheduling_Model.Constants.EnSemesterType;

public class Course 
{
	public int number;
	public Course[] prerequisites = new Course[0];
	public Semester[] semestersOffered;
	public EnSemesterType whenOffered;
	
	public Course(int courseNum, EnSemesterType type)
	{
		whenOffered = type;
		number = courseNum;
	}
	
	public void PopulateSemestersOffered(Semester[] semesters)
	{
		ArrayList<Semester> semestersOfferedList = new ArrayList<Semester>();
		for(int i = 0; i<Constants.NUM_SEMESTERS; i++)
		{
			if(whenOffered == EnSemesterType.All || whenOffered == semesters[i].getSemesterType())
				semestersOfferedList.add(semesters[i]);
		}
		semestersOffered = (Semester[]) semestersOfferedList.toArray(new Semester[semestersOfferedList.size()]);
	}
	
	public void populatePrerequisiteList(Hashtable<Integer, Course> courseList)
	{
		ArrayList<Course> prereqs = new ArrayList<Course>();
		if(ConstraintDefinitions.Prerequisites.containsKey(number))
		{
			for(Integer prereq : ConstraintDefinitions.Prerequisites.get(number))
			{
				prereqs.add(courseList.get(prereq));
			}
			prerequisites = (Course[]) prereqs.toArray(new Course[prereqs.size()]);
		}
	}
}
