package main.Scheduling_Model;

import java.util.Hashtable;

import main.Scheduling_Model.Constants.EnSemesterType;

public class ConstraintDefinitions 
{

	
	public static final int[] RegularCourses = 	{ 2, 3, 4, 6, 8, 9, 12, 13 };
	public static final int[] FallCourses = 	{ 1, 7, 11, 15, 17 };
	public static final int[] SpringCourses = 	{ 5, 10, 14, 16, 18 };
	public static final int[] SummerCourses = 	{	};  // No summer only courses
	
	
	// Maps a course id to an array of it's prerequisites
	@SuppressWarnings("serial")
	public static final Hashtable<Integer,EnSemesterType> SemestersOffered = new Hashtable<Integer, EnSemesterType>()
	{{    	   
		    put(1,  EnSemesterType.Fall);
		    put(2,  EnSemesterType.All);
		    put(3,  EnSemesterType.All);
		    put(4,  EnSemesterType.All);
		    put(5,  EnSemesterType.Spring);
		    put(6,  EnSemesterType.All);
		    put(7,  EnSemesterType.Fall);
		    put(8,  EnSemesterType.All);
		    put(9,  EnSemesterType.All);
		    put(10, EnSemesterType.Spring);
		    put(11, EnSemesterType.Fall);
		    put(12, EnSemesterType.All);
		    put(13, EnSemesterType.All);
		    put(14, EnSemesterType.Spring);
		    put(15, EnSemesterType.Fall);
		    put(16, EnSemesterType.Spring);
		    put(17, EnSemesterType.Fall);
		    put(18, EnSemesterType.Spring);
	}}; 
	
	// Maps a course id to an array of it's prerequisites
	@SuppressWarnings("serial")
	public static final Hashtable<Integer,Integer[]> Prerequisites = new Hashtable<Integer, Integer[]>()
	{{    	   
		    put(16,      new Integer[]{4});
		    put(1,       new Integer[]{12});
		    put(13,      new Integer[]{9});  
		    put(7,       new Integer[]{3});  
	}}; 
	
}
