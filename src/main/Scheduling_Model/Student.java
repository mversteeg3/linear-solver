package main.Scheduling_Model;

public class Student 
{
	int number;
	Course[] desiredCourses;
	
	public Student(int num)
	{
		number = num;
	}
	
	public Boolean RequiresCourse(int courseToCheck)
	{
		for(Course c : desiredCourses)
		{
			if(c.number == courseToCheck)
				return true;
		}
		return false;
	}
}
