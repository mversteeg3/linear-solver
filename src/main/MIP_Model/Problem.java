package main.MIP_Model;

import java.util.ArrayList;

public abstract class Problem 
{
	public String name;
	public ArrayList<Variable> variables;
	public ArrayList<Constraint> constraints;
	public Objective objective;
    
    public Problem(String n)
    {
    	name = n;
    	constraints = new ArrayList<Constraint>();
    	variables = new ArrayList<Variable>();
    	objective = new Objective();
    }
   
    
}
