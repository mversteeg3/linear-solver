package main.MIP_Model;

import java.util.ArrayList;

public class Objective 
{
	ArrayList<Term> terms;
    Common.EnOptimization optimization;
    
    public Objective()
    {
    	this(null);
    }
    
    public Objective(Common.EnOptimization opt)
    {
    	terms = new ArrayList<Term>();
    	optimization = opt;
    }
    
    public void AddTerm(double coeff, Variable var)
    {
    	AddTerm(new Term(coeff, var));
    }
    
    public void AddTerm(Term t)
    {
    	terms.add(t);
    }
    
    public Term[] getTerms()
    {
    	return terms.toArray(new Term[terms.size()]);
    }
    
    public Common.EnOptimization getOptimizationGoal()
    {
    	return optimization;
    }
    
}
