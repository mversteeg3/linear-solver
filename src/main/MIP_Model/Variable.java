package main.MIP_Model;

import main.MIP_Model.Common.EnVariableType;

public abstract class Variable 
{
	public String name;
	public double lowerBound;
    public double upperBound;
    public EnVariableType type;
    public double objective;
    
    protected Variable(String n)
    {
    	name = n;
    }
    
    protected Variable(String n, double lb, double ub, double sln)
    {
    	lowerBound = lb;
    	upperBound = ub;
    	objective = sln;
    	name = n;
    }
}
