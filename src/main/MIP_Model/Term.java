package main.MIP_Model;

public class Term 
{
	double coefficient;
    Variable variable;
    
    public Term(double coeff, Variable var)
    {
    	coefficient = coeff;
    	variable = var;
    }
    
    public double getCoefficient()
    {
    	return coefficient;
    }
    
    public String getVariableName()
    {
    	return variable.name;
    }
}
