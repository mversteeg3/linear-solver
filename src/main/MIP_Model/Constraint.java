package main.MIP_Model;

import java.util.ArrayList;

public class Constraint 
{
	String name;
    double rightSide;
    Common.EnComparator comparator;
    ArrayList<Term> terms;
    
    public Constraint(String n, double rhs, Common.EnComparator comp)
    {
    	name = n;
    	rightSide = rhs;
    	comparator = comp;
    	terms = new ArrayList<Term>();
    }
    
    public void AddTerm(double coeff, Variable var)
    {
    	AddTerm(new Term(coeff, var));
    }
    
    public void AddTerm(Term t)
    {
    	terms.add(t);
    }
    
    public Term[] getTerms()
    {
    	return terms.toArray(new Term[terms.size()]);
    }
    
    public double getRHS()
    {
    	return rightSide;
    }
    
    public Common.EnComparator getComparator()
    {
    	return comparator;
    }
    
    public String getName()
    {
    	return name;
    }
}

