package main.MIP_Model;

import main.MIP_Model.Common.EnVariableType;

public class BooleanVariable extends Variable 
{
	
	public BooleanVariable(String n)
	{
		super(n);
		type = EnVariableType.Boolean;
		lowerBound = 0;
		upperBound = 1;
		objective = 0;
	}
}
