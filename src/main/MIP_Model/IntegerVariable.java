package main.MIP_Model;

import main.MIP_Model.Common.EnVariableType;

public class IntegerVariable extends Variable
{
	Boolean bounded = false;
	public IntegerVariable(String n) 
	{
		super(n);
		type = EnVariableType.Integer;
		objective = 0;
	}
	public IntegerVariable(String n, double lb, double ub) 
	{
		this(n);
		bounded = true;
		lowerBound = lb;
		upperBound = ub;
		bounded = true;
	}

}
