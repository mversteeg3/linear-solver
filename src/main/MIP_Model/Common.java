package main.MIP_Model;

public class Common 
{
	public enum EnVariableType
	{
		Integer,
		Continuous,
		Boolean
	};
	
	public enum EnComparator
	{
		GreaterThanOrEqualTo,
		LessThanOrEqualTo,
		EqualTo
	};
	
	public enum EnOptimization
	{
		Maximize,
		Minimize
	};

}
